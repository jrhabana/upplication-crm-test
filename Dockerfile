FROM upplication/openjdk8alpine
VOLUME /tmp
ADD build/libs/fenix-*.jar app.jar
ENV JAVA_OPTS=""
RUN apk upgrade
RUN apk add --update imagemagick
ENTRYPOINT exec java $SPRING_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar