(function (window, $) {
    'use strict';

    $(function () {

        var parseDate = function(date){
            return new Date(date).toLocaleDateString('es-ES', {
                day : 'numeric',
                month : 'short',
                year : 'numeric'
            });
        };

        $('.collapse').on('show.bs.collapse', function () {
            var $content = $(this).find('.container .resume');
            var id = this.getAttribute('data-id');
            $content.find(".childupp-empty").addClass("hidden");
            $content.find(".childupp-request-empty").addClass("hidden");
            $.get(contextPath + "webusers/" + id + "/childupps").done(function(data){

                $content.find(".childupp .elem:not(.example)").remove();

                if (data.childUpps.length > 0) {
                    $content.find(".childupp").removeClass("hidden");
                    $content.find(".childupp-empty").addClass("hidden");
                    data.childUpps.forEach(function(elem) {
                        var $childupp = $content.find(".childupp .elem.example").clone();
                        $childupp.removeClass("example").removeClass("hidden");
                        $childupp.find(".id").append(elem.id);
                        $childupp.find(".created").append(parseDate(elem.created));
                        $childupp.find(".name").append(elem.name);
                        $childupp.find(".pricing-plan").append(elem.pricingPlan);
                        if (elem.deleted) {
                            $childupp.find(".deleted").append(parseDate(elem.deleted));
                        }
                        $content.find(".childupp .list-group").append($childupp);
                    });

                    $content.find(".childupp-request .elem:not(.example)").remove();
                } else {
                    $content.find(".childupp").addClass("hidden");
                    $content.find(".childupp-empty").removeClass("hidden");
                }

                $content.find(".childupp-request .elem:not(.example)").remove();

                if (data.requests.length > 0) {
                    $content.find(".childupp-request").removeClass("hidden");
                    $content.find(".childupp-request-empty").addClass("hidden");
                    data.requests.forEach(function(elem){
                        var $childUppRequest = $content.find(".childupp-request .elem.example").clone();
                        $childUppRequest.removeClass("example").removeClass("hidden");
                        $childUppRequest.find(".id").append(elem.id);
                        $childUppRequest.find(".created").append(parseDate(elem.created));
                        $childUppRequest.find(".pricing-plan").append(elem.pricingPlan);
                        $childUppRequest.find(".type").append(elem.type);
                        if (elem.resolved) {
                            $childUppRequest.find(".resolved").append(parseDate(elem.resolved));
                        }
                        $content.find(".childupp-request .elem.example").after($childUppRequest);
                    });

                    $content.find(".childupp-request .badge.total").empty();
                    $content.find(".childupp-request .badge.total").append(data.requests.length);
                } else {
                    $content.find(".childupp-request").addClass("hidden");
                    $content.find(".childupp-request-empty").removeClass("hidden");
                }
            });
        });
    });

}(window, jQuery));