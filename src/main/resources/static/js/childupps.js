(function (window, j) {

    function showSuccess($container) {
        $container.find('.alert-success').show();
        $container.find('.alert-danger').hide();
    }

    function showError($container) {
        $container.find('.alert-danger').show();
        $container.find('.alert-success').hide();
    }

    function hideAlerts($container) {
        $container.find('.alert').hide();
    }

    function transformData(data) {
        var result = {};
        j.each(data, function() {
            if (result[this.name] !== undefined) {
                if (!result[this.name].push) {
                    result[this.name] = [result[this.name]];
                }
                result[this.name].push(this.value || '');
            } else {
                result[this.name] = this.value || '';
            }
        });

        return result;
    }

    function onPromoSubmit($modal) {
        return function (e) {
            e.preventDefault();

            var form = this;
            var url = this.action;
            var data = j(form).serializeArray();

            j.ajax({
                method: 'POST',
                data: JSON.stringify(transformData(data)),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                url: url,
                success: function () {
                    window.setTimeout(function () {
                        $modal.modal('hide');
                    }, 3000);

                    showSuccess($modal);
                },
                error: function () {
                    showError($modal);
                }
            });
        };
    }

    function onModalShown(e) {
        var button = e.relatedTarget;
        var id = button.getAttribute('data-childupp-id');
        var name = button.getAttribute('data-childupp-name');
        var $modal = j(this);

        $modal.find('.modal-title').text('Change promotion of ' + name);
        $modal.find('#childupp-id').val(id);
        $modal.find('#promotion-code').val('');

        hideAlerts($modal);
    }

    j(function () {
        var $modal = j('#modal-change-promotion');
        $modal.on('show.bs.modal', onModalShown);
        $modal.find('form').on('submit', onPromoSubmit($modal));
    });

}(window, jQuery));