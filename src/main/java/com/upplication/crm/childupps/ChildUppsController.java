package com.upplication.crm.childupps;

import com.upplication.crm.support.web.PageWrapper;
import es.upplication.entities.type.CustomerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ChildUppsController {

    @Autowired
    private ChildUppManager childUppManager;

    @Value("${upplication.childupp-api.core-url}")
    private String apiUrl;

    private final static String CHILDUPPS_VIEW = "childupps/index";

    @RequestMapping(value = "childupps", method = RequestMethod.GET)
    public String requests(Model model,
                           @PageableDefault(sort = {"creationDate"}, direction = Sort.Direction.DESC, value = 20) Pageable pageable,
                           @RequestParam(value = "appName", required = false) String appName,
                           @RequestParam(value = "appId", required = false) Integer appId,
                           @RequestParam(value = "deleted", required = false, defaultValue = "false") Boolean deleted,
                           @RequestParam(value = "customerType", required = false, defaultValue = "UPPLICATION") CustomerType customerType) {
        PageWrapper<App> page = new PageWrapper<>(childUppManager.getApps(pageable, appName, appId != null ? appId : 0, customerType, deleted), "childupps");

        model.addAttribute("page", page);
        model.addAttribute("appName", appName);
        model.addAttribute("appId", appId);
        model.addAttribute("customerType", customerType);
        model.addAttribute("deleted", deleted);
        model.addAttribute("changePromotionEndpoint", apiUrl + "billing/change-promotion");

        return CHILDUPPS_VIEW;
    }

}
