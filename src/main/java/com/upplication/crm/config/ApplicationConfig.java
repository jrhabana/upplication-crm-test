package com.upplication.crm.config;

import es.upplication.security.TokenAdminLogin;
import es.upplication.security.TokenLogin;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
public class ApplicationConfig {

	private static final String MESSAGE_SOURCE = "classpath:/i18n/messages";

    @Bean
    public static TokenAdminLogin tokenAdminLogin(){
        return new TokenAdminLogin();
    }

	@Bean
	public static TokenLogin tokenAplicatecaLogin(){
		return new TokenLogin();
	}

	@Bean(name = "messageSource")
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename(MESSAGE_SOURCE);
		messageSource.setCacheSeconds(5);
		return messageSource;
	}
}