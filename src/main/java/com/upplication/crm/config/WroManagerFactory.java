package com.upplication.crm.config;

import org.springframework.core.io.ClassPathResource;
import ro.isdc.wro.http.ConfigurableWroFilter;
import ro.isdc.wro.manager.factory.ConfigurableWroManagerFactory;
import ro.isdc.wro.model.factory.WroModelFactory;
import ro.isdc.wro.model.factory.XmlModelFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * We extends ConfigurableWroManagerFactory so the preprocessor and postprocessor are getting from wro.properties
 * @see ConfigurableWroFilter#newWroManagerFactory()
 * @see ro.isdc.wro.manager.factory.DefaultWroManagerFactory#initFactory(Properties)
 */
public class WroManagerFactory extends ConfigurableWroManagerFactory implements ro.isdc.wro.manager.factory.WroManagerFactory {

    @Override
    protected WroModelFactory newModelFactory() {
        return new XmlModelFactory() {
            @Override
            protected InputStream getModelResourceAsStream() throws IOException {
                final String resourceLocation = getDefaultModelFilename();
                final InputStream stream = new ClassPathResource("/wro/wro.xml").getInputStream();

                if (stream == null) {
                    throw new IOException("Invalid resource requested: " + resourceLocation);
                }

                return stream;
            }
        };
    }
}
