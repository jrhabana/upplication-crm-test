package com.upplication.crm.config;

import javax.sql.DataSource;

import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

@Configuration
class JpaConfig {

    private final DataSource dataSource;

    public JpaConfig(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            EntityManagerFactoryBuilder builder) {
        // I dont know why @EntityScan doesnt work and wee need to configure manually
        return builder.dataSource(dataSource).packages("es.upplication.entities").build();
    }


}
