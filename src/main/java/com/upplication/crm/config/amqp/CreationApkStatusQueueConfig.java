package com.upplication.crm.config.amqp;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class CreationApkStatusQueueConfig {

    // Params for Google Play Creation APK Environment Queue
    @Autowired
    private Environment environment;

    @Bean
    public TopicExchange creationApkStatusExchange() {
        return new TopicExchange(environment.getProperty("rabbitmq.apk.creationStatus.exchange"));
    }

    @Bean
    public Queue creationApkStatusQueue() { return new Queue(environment.getProperty("rabbitmq.apk.creationStatus.queue"), true); }

    @Bean
    public Binding creationApkStatusDeclareBinding() {
        return BindingBuilder.bind(creationApkStatusQueue()).to(creationApkStatusExchange()).with(environment.getProperty("rabbitmq.apk.creationStatus.routingKey"));
    }

}
