package com.upplication.crm.config.amqp;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class CreationIpaQueueConfig {

    // Params for Creation IPA Environment
    @Autowired
    private Environment environment;

    @Bean
    public TopicExchange creationIpaExchange() {
        return new TopicExchange(environment.getProperty("rabbitmq.ipa.creation.exchange"));
    }

    @Bean
    public Queue creationIpaQueue() { return new Queue(environment.getProperty("rabbitmq.ipa.creation.queue"), true); }

    @Bean
    public Binding creationIpaDeclareBinding() {
        return BindingBuilder.bind(creationIpaQueue()).to(creationIpaExchange()).with(environment.getProperty("rabbitmq.ipa.creation.routingKey"));
    }

}
