package com.upplication.crm.config;

import com.upplication.push.ApiEnv;
import com.upplication.push.PushAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PushApiConfig {

    @Value("${upplication.push.authorization-token}")
    private String authorizationToken;
    @Value("${upplication.push.environment}")
    private String environment;

    @Bean
    public PushAPI pushApi() {
        PushAPI api = new PushAPI(authorizationToken);
        if (environment.equals("develop"))
            api.withEnv(ApiEnv.DEVELOP);
        return api;
    }
}