package com.upplication.crm.market.requests.domain;


import java.util.List;

public class CreationApkMessage {

    private int childUppId;
    private String bundleId;
    private String name;
    private int version;
    private String iconUrl;
    private int requestId;
    private boolean debug;
    private String webApp;
    private List<String> pluginsExtra;
    // Sign values
    private String signatureKeyAlias;
    private String signaturePasswordAlias;
    private String signaturePasswordStore;
    private String signatureStoreUri;

    private String errorMsg;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(int childUppId) {
        this.childUppId = childUppId;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getWebApp() {
        return webApp;
    }

    public void setWebApp(String webApp) {
        this.webApp = webApp;
    }

    public String getSignatureKeyAlias() {
        return signatureKeyAlias;
    }

    public void setSignatureKeyAlias(String signatureKeyAlias) {
        this.signatureKeyAlias = signatureKeyAlias;
    }

    public String getSignaturePasswordAlias() {
        return signaturePasswordAlias;
    }

    public void setSignaturePasswordAlias(String signaturePasswordAlias) {
        this.signaturePasswordAlias = signaturePasswordAlias;
    }

    public String getSignaturePasswordStore() {
        return signaturePasswordStore;
    }

    public void setSignaturePasswordStore(String signaturePasswordStore) {
        this.signaturePasswordStore = signaturePasswordStore;
    }

    public String getSignatureStoreUri() {
        return signatureStoreUri;
    }

    public void setSignatureStoreUri(String signatureStoreUri) {
        this.signatureStoreUri = signatureStoreUri;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<String> getPluginsExtra() {
        return pluginsExtra;
    }

    public void setPluginsExtra(List<String> pluginsExtra) {
        this.pluginsExtra = pluginsExtra;
    }
}
