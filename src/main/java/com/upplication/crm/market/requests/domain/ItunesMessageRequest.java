package com.upplication.crm.market.requests.domain;


public class ItunesMessageRequest extends PublishRequestMessage {

    private String bundleId;
    private int version;
    private String ipaUrl;
    private String tokenUsername;
    private String tokenPassword;
    private String tokenTeamId;
    private String tokenTeamName;
    private String companyName;

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getIpaUrl() {
        return ipaUrl;
    }

    public void setIpaUrl(String ipaUrl) {
        this.ipaUrl = ipaUrl;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getTokenUsername() {
        return tokenUsername;
    }

    public void setTokenUsername(String tokenUsername) {
        this.tokenUsername = tokenUsername;
    }

    public String getTokenPassword() {
        return tokenPassword;
    }

    public void setTokenPassword(String tokenPassword) {
        this.tokenPassword = tokenPassword;
    }

    public String getTokenTeamId() {
        return tokenTeamId;
    }

    public void setTokenTeamId(String tokenTeamId) {
        this.tokenTeamId = tokenTeamId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTokenTeamName() {
        return tokenTeamName;
    }

    public void setTokenTeamName(String tokenTeamName) {
        this.tokenTeamName = tokenTeamName;
    }
}
