package com.upplication.crm.market.requests.util;

import com.upplication.crm.market.requests.domain.*;
import com.upplication.crm.repository.*;
import es.upplication.entities.*;
import es.upplication.type.OperatingSystemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.upplication.crm.repository.ChildUppOsSpecification.filterByChildUppIdAndOs;
import static com.upplication.crm.repository.FeaturesSpecification.filterByChildUppId;
import static com.upplication.crm.repository.ScreenshotSpecification.filterByRequestId;
import static org.springframework.data.jpa.domain.Specifications.where;

@Component
public class BuilderMessageUtil {

    Logger logger = LoggerFactory.getLogger(BuilderMessageUtil.class);

    private final ScreenshotRepository screenshotRepository;
    private final ChildUppOsRepository childUppOsRepository;
    private final FeaturesRepository featuresRepository;
    private final AppleSignRepository appleSignRepository;
    private final MarketRequestRepository marketRequestRepository;
    private final ChildUppRepository childUppRepository;
    private final FriendlyUrl friendlyUrl;
    private final GooglePlaySignRepository googlePlaySignRepository;

    public BuilderMessageUtil(final ScreenshotRepository screenshotRepository, final ChildUppOsRepository childUppOsRepository,
                              final FeaturesRepository featuresRepository, final AppleSignRepository appleSignRepository,
                              final MarketRequestRepository marketRequestRepository, final ChildUppRepository childUppRepository,
                              final FriendlyUrl friendlyUrl, final GooglePlaySignRepository googlePlaySignRepository) {
        this.screenshotRepository = screenshotRepository;
        this.childUppOsRepository = childUppOsRepository;
        this.featuresRepository = featuresRepository;
        this.appleSignRepository = appleSignRepository;
        this.marketRequestRepository = marketRequestRepository;
        this.childUppRepository = childUppRepository;
        this.friendlyUrl = friendlyUrl;
        this.googlePlaySignRepository = googlePlaySignRepository;
    }

    /**
     * Transform the form object to specific message object to send to the queue exchange
     *
     * @param request Form object with all the information to transform
     * @param bundleId Bundle identifier of the App
     * @param ipaUrl Url with the IPA to send to the Itunes Worker
     * @return New object with the necessary information to send to the queue exchange
     */
    public ItunesMessageRequest buildItunesMessageToSend(PublishRequest request, String bundleId, String ipaUrl) {
        logger.info("Building Itunes Publish message to send for Bundle ID "+bundleId);
        ItunesMessageRequest message = new ItunesMessageRequest();
        message.setRequestId(request.getId());
        message.setApp(request.getChildUpp().getId());
        message.setName(request.getName());

        logger.info("Bundle ID message to send: "+ bundleId);
        message.setBundleId(bundleId);
        message.setDescription(request.getDescription());
        message.setLanguage(request.getLanguage());
        message.setKeywords(request.getKeywords());
        message.setVideoPath(request.getVideoPath());
        message.setChanges(request.getChanges());
        message.setContent(request.getContent());
        message.setCategory(request.getCategory());
        message.setIcon(request.getIcon());

        List<ScreenshotMessage> screenshots = buildScreenshotsMessage(request.getId());
        message.setScreenshots(screenshots);

        ChildUppOS childUppOS = childUppOsRepository.findOne(where(filterByChildUppIdAndOs(request.getChildUpp().getId(), OperatingSystemType.IOS.getValue())));
        logger.info("Version to send to Itunes Publish: "+childUppOS.getVersion()+ " IPA url: "+ipaUrl);
        message.setVersion(childUppOS.getVersion());
        message.setIpaUrl(ipaUrl);

        message.setSupportUrl(request.getSupportUrl());
        message.setCopyright(request.getCopyright());
        message.setContactName(request.getContactName());
        message.setContactSurname(request.getContactSurname());
        message.setContactPhone(request.getContactPhone());
        message.setContactEmail(request.getContactEmail());

        // Sign properties
        populateAppleSign(request.getId(), message);

        return message;
    }

    /**
     * Transform the form object to specific message object to send to the queue exchange
     *
     * @param request Form object with all the information to transform
     * @return New object with the necessary information to send to the queue exchange
     */
    public CreationIpaMessage buildIpaMessage(PublishRequestForm request) {

        ChildUppOS childUppOS = childUppOsRepository.findOne(where(filterByChildUppIdAndOs(request.getApp(), OperatingSystemType.IOS.getValue())));
        HistoricFeatures feature = featuresRepository.findOne(where(filterByChildUppId(request.getApp())));

        CreationIpaMessage message = new CreationIpaMessage();
        message.setChildUppId(request.getApp());
        message.setBundleId(request.getIosBundleId());
        message.setName(request.getName());
        logger.info("Searching childUpp os for childUppId " + request.getApp() + " and Operating system iOS");
        int nextVersion = childUppOS.getVersion() + 1;
        logger.info("Sending to build version " + nextVersion);
        message.setVersion(nextVersion);
        message.setIconUrl(request.getIcon());
        message.setRequestId(request.getRequestId());
        message.setSplashWithAds(feature.isAdvertising());

        if (request.getPluginsExtra() != null) {
            logger.info("Found extra plugins: " + request.getPluginsExtra());
            message.setPluginsExtra(Arrays.asList(request.getPluginsExtra().split(",")));
        }

        // Sign properties
        AppleSign appleSign = appleSignRepository.findOne(marketRequestRepository.findOne(request.getRequestId()).getAppleSign().getId());
        logger.info("Building IPA Message to send to the worker with Apple Sign ID " + appleSign.getId());

        // apple credentials
        message.setUsername(appleSign.getUsername());
        message.setPassword(appleSign.getPassword());
        // Team Name is used as Team identifier (API login)
        message.setTeamName(appleSign.getTeamName());
        message.setTeamId(appleSign.getTeamId());
        // fields to build the ipa if doesnt want to use publisherApi
        message.setCertificate(appleSign.getCertificateUrl());
        message.setTypeCertificate(appleSign.getEnvironment().getValue());
        message.setPasswordCertificate(appleSign.getCertificatePsw());
        message.setMobileProvision(appleSign.getProvisionUrl());
        // auth key for apple push
        message.setAuthKeyId(appleSign.getAuthKeyId());
        message.setAuthKeyContent(appleSign.getAuthKeyContent());

        return message;
    }

    /**
     * Transform the form object to specific message object to send to the queue exchange
     *
     * @param request Form object with all the information to transform
     * @return New object with the necessary information to send to the queue exchange
     */
    public CreationApkMessage buildApkMessage(PublishRequestForm request) {
        logger.info("Building APK message to send");
        CreationApkMessage message = new CreationApkMessage();
        message.setChildUppId(request.getApp());
        message.setBundleId(request.getAndroidBundleId());
        message.setName(request.getName());

        logger.info("Searching ChildUpp OS for ChildUpp ID " + request.getApp() + " and Operating system Android");
        ChildUppOS childUppOS = childUppOsRepository.findOne(where(filterByChildUppIdAndOs(request.getApp(), OperatingSystemType.ANDROID.getValue())));
        message.setVersion(childUppOS.getVersion() + 1);

        message.setIconUrl(request.getIcon());
        message.setRequestId(request.getRequestId());
        message.setDebug(request.isDebugAndroid());

        ChildUpp childUpp = childUppRepository.findOne(request.getApp());
        logger.info("Build webapp domain for this ChildUpp " + childUpp.getId());
        String webApp = friendlyUrl.getSubdomainFromName(request.getName());
        webApp = friendlyUrl.getURL(webApp, childUpp.getOwner().getCustomerType()).getHost();
        message.setWebApp(webApp);

        if (request.getPluginsExtra() != null) {
            logger.info("Found extra plugins: " + request.getPluginsExtra());
            message.setPluginsExtra(Arrays.asList(request.getPluginsExtra().split(",")));
        }

        logger.info("Buscando request ID: "+request.getRequestId());
        // Sign values
        GooglePlaySign googlePlaySign = googlePlaySignRepository.findOne(marketRequestRepository.findOne(request.getRequestId()).getGooglePlaySign().getId());
        logger.info("Building APK Message to send to the worker with Google Play Sign ID " + googlePlaySign.getId());
        message.setSignatureKeyAlias(googlePlaySign.getSignatureKeyAlias());
        message.setSignaturePasswordAlias(googlePlaySign.getSignaturePasswordAlias());
        message.setSignaturePasswordStore(googlePlaySign.getSignaturePasswordStore());
        message.setSignatureStoreUri(googlePlaySign.getSignatureStoreUri());

        return message;
    }

    /**
     * Transform the form object to specific message object to send to the queue exchange
     *
     * @param request Form object with all the information to transform
     * @return New object with the necessary information to send to the queue exchange
     */
    public ItunesMessageRequest buildItunesPublishMessage(PublishRequestForm request) {
        ItunesMessageRequest message = new ItunesMessageRequest();

        message.setRequestId(request.getRequestId());
        message.setApp(request.getApp());
        message.setName(request.getName());

        message.setDescription(request.getDescription());
        message.setLanguage(request.getLanguage());
        message.setKeywords(request.getKeywords());
        message.setVideoPath(request.getVideoPath());
        message.setChanges(request.getChanges());
        message.setContent(request.getContent());
        message.setCategory(request.getCategory());
        message.setIcon(request.getIcon());

        List<ScreenshotMessage> screenshots = buildScreenshotsMessage(request.getRequestId());
        message.setScreenshots(screenshots);

        message.setSupportUrl(request.getSupportUrl());
        message.setCopyright(request.getCopyright());
        message.setContactName(request.getContactName());
        message.setContactSurname(request.getContactSurname());
        message.setContactPhone(request.getContactPhone());
        message.setContactEmail(request.getContactEmail());

        logger.info("Bundle ID message to send: "+ request.getIosBundleId());
        logger.info("Version to send to Itunes Publish..."+request.getIosVersion());
        message.setBundleId(request.getIosBundleId());
        message.setVersion(request.getIosVersion());
        message.setIpaUrl(request.getIpaUrl());

        populateAppleSign(request.getRequestId(), message);

        return message;
    }

    /**
     * Fullfil PublishRequestIosMessage message with the apple sign
     * attached to the PublishRequest
     *
     * @param requestId int, mandatory, the request id
     * @param message PublishRequestIosMessage, mandatory, not null
     */
    private void populateAppleSign(int requestId, ItunesMessageRequest message) {
        // Sign properties
        AppleSign appleSign = appleSignRepository.findOne(marketRequestRepository.findOne(requestId).getAppleSign().getId());
        logger.info("Building IPA Message to send to the worker with Apple Sign ID " + appleSign.getId());
        message.setTokenUsername(appleSign.getUsername());
        message.setTokenPassword(appleSign.getPassword());
        message.setTokenTeamId(appleSign.getTeamId());
        logger.info("Setting Company Name " + appleSign.getCompanyName()+ " and TEAM Name " + appleSign.getTeamName());
        message.setTokenTeamName(appleSign.getTeamName());
        message.setCompanyName(appleSign.getCompanyName());
    }

    /**
     * Get list of screenshots for the specific Publish Request ID
     * with the message data to send to a queue exchange
     *
     * @param requestId Publish Request ID to search
     * @return List of Screenshot message to send to the queue
     */
    private List<ScreenshotMessage> buildScreenshotsMessage(int requestId) {
        logger.info("Building screenshots");
        List<Screenshot> screenshots = screenshotRepository.findAll(where(filterByRequestId(requestId)));
        logger.info("Found screenshots: "+screenshots.size());
        return screenshots.stream().map(screenshot -> {
            ScreenshotMessage screenshotMessage = new ScreenshotMessage();
            screenshotMessage.setId(screenshot.getId());
            screenshotMessage.setRequestId(screenshot.getRequestId().getId());
            screenshotMessage.setPosition(screenshot.getPosition());
            screenshotMessage.setDevice(screenshot.getDeviceType());
            screenshotMessage.setUrl(screenshot.getUrl());
            return screenshotMessage;
        }).collect(Collectors.toList());
    }

}
