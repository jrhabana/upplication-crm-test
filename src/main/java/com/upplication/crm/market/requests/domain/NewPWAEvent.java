package com.upplication.crm.market.requests.domain;


import java.io.Serializable;
import java.net.URI;

public class NewPWAEvent implements Serializable {

    private URI url;
    private int childUppId;

    public NewPWAEvent() { }

    public NewPWAEvent(URI url, int childUppId) {
        this.url = url;
        this.childUppId = childUppId;
    }


    public URI getUrl() {
        return url;
    }

    public void setUrl(URI url) {
        this.url = url;
    }


    public int getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(int childUppRequest) {
        this.childUppId = childUppRequest;
    }

    @Override
    public String toString() {
        return "PWAIcon{" +
                "url='" + url + '\'' +
                ", childUppId=" + childUppId +
                '}';
    }
}
