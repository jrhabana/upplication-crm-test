package com.upplication.crm.market.requests.domain;


import java.io.Serializable;
import java.net.URI;

public class ChildUppInactiveEvent implements Serializable {

    private int childUppId;

    public ChildUppInactiveEvent() { }

    public ChildUppInactiveEvent(URI url, int childUppId) {
        this.childUppId = childUppId;
    }


    public int getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(int childUppRequest) {
        this.childUppId = childUppRequest;
    }

    @Override
    public String toString() {
        return "ChildUppInactiveEvent {" +
                "childUppId=" + childUppId +
                '}';
    }
}
