package com.upplication.crm.market.requests.email;

import com.upplication.crm.market.requests.domain.AppPublication;
import com.upplication.crm.repository.SettingsRepository;
import es.upplication.entities.Settings;
import es.upplication.mail.Sender;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.StringWriter;


@Component
public class PublishRequestEmail {

    Logger logger = LoggerFactory.getLogger(PublishRequestEmail.class);

    private final VelocityEngine velocityEngine;

    private final SettingsRepository settingsRepository;

    private final String appPublishedSubjectEn;
    private final String appPublishedSubjectEs;

    private final Sender sender;

    public PublishRequestEmail( final VelocityEngine velocityEngine, final SettingsRepository settingsRepository,
                               @Value("${upplication.emails.published.subject.en}") final String appPublishedSubjectEn,
                               @Value("${upplication.emails.published.subject.es}") final String appPublishedSubjectEs, final Sender sender) {
        this.settingsRepository = settingsRepository;
        this.velocityEngine = velocityEngine;
        this.appPublishedSubjectEn = appPublishedSubjectEn;
        this.appPublishedSubjectEs = appPublishedSubjectEs;
        this.sender = sender;
    }

    /**
     * Sends an email to the creator to notify that his/her app has been published on the Apple Store or Google Play
     *
     * @param notification Data of the notification
     */
    public void notifyCreatorAppPublished(AppPublication notification) {

        VelocityContext context = new VelocityContext();
        context.put("appName", notification.getAppName());
        context.put("os", notification.getOs());
        context.put("name", notification.getUserName());
        context.put("marketUrl", notification.getMarketUrl());

        Settings setting = settingsRepository.findOne(
                (root, query, cb) ->
                       cb.equal(root.join("organization").get("name"), notification.getOrganizationName())
        );

        String host = setting.getEmailHost();
        String username = setting.getEmailUserName();
        String pwd = setting.getEmailPassword();
        String smtpPort = setting.getEmailSmtpPort();
        String from = setting.getNoReplyEmail();

        String lang = notification.getUserLang();
        String userEmail = notification.getUserMail();
        logger.info("Sending email to the Creator "+userEmail+ "with language "+lang);

        String msg = buildTemplate(lang, context);
        String emailSubject = appPublishedSubjectEn;
        if (lang.equalsIgnoreCase("es")) {
            emailSubject = appPublishedSubjectEs;
        }

        sender.sendMessage(from, userEmail, null, emailSubject, msg, host, username, pwd, smtpPort);
    }

    /**
     * Build the email template with the passed context
     *
     * @param lang Language to select specific template email file from resources
     * @param context      Email context
     * @return Email message
     */
    private String  buildTemplate(String lang, VelocityContext context) {
        StringWriter out = new StringWriter();
        Template template = velocityEngine.getTemplate("emails/en/email-app-published.vm");
        if (lang.equalsIgnoreCase("es")) {
            template = velocityEngine.getTemplate("emails/email-app-published.vm");
        }
        template.merge(context, out);
        return out.toString();
    }

}
