package com.upplication.crm.market.requests.domain;

import es.upplication.entities.type.ScreenshotDeviceType;


public class ScreenshotMessage {

    private int id;

    private int requestId;

    private int position;

    private ScreenshotDeviceType device;

    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ScreenshotDeviceType getDevice() {
        return device;
    }

    public void setDevice(ScreenshotDeviceType device) {
        this.device = device;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
