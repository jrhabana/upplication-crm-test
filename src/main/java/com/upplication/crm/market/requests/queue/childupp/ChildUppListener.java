package com.upplication.crm.market.requests.queue.childupp;

import com.rabbitmq.client.Channel;
import com.upplication.crm.market.requests.domain.ChildUppInactiveEvent;
import com.upplication.crm.market.requests.domain.ChildUppRestoreEvent;
import com.upplication.crm.market.requests.domain.NewPWAEvent;
import com.upplication.crm.market.requests.s3.AwsS3Service;
import com.upplication.crm.repository.ChildUppOsRepository;
import com.upplication.crm.repository.ChildUppRepository;
import com.upplication.crm.util.ImageMagickTransformer;
import com.upplication.push.PushAPI;
import com.upplication.push.domain.request.RequestApp;
import com.upplication.push.domain.response.ResponseApp;
import es.upplication.entities.ChildUpp;
import es.upplication.entities.ChildUppOS;
import es.upplication.entities.type.StatusRequest;
import es.upplication.type.OperatingSystemType;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.upplication.crm.repository.ChildUppOsSpecification.filterByChildUppIdAndOs;

@Component
public class ChildUppListener {

    private final ChildUppOsRepository childUppOsRepository;
    private final ChildUppRepository childUppRepository;
    private final PushAPI pushAPI;
    private final AwsS3Service awsS3Service;
    private final ImageMagickTransformer imageMagickTransformer;

    private List<Icon> icons = Arrays.asList(new Icon(192, 192), new Icon(256,256), new Icon(384, 384), new Icon(512, 512));

    private static final Logger logger = LoggerFactory.getLogger(ChildUppListener.class);

    public ChildUppListener(final ChildUppOsRepository childUppOsRepository,
                            final ChildUppRepository childUppRepository,
                            final AwsS3Service awsS3Service,
                            final ImageMagickTransformer imageMagickTransformer,
                            final PushAPI pushAPI) {
        this.childUppOsRepository = childUppOsRepository;
        this.childUppRepository = childUppRepository;
        this.awsS3Service = awsS3Service;
        this.imageMagickTransformer = imageMagickTransformer;
        this.pushAPI = pushAPI;
    }

    /**
     * Method to listen the new PWA Icon Queue and set the icon for the PWA.
     *
     * @param pwa NewPWAEvent icon response all the information with the worker result
     */
    @RabbitListener(queues = "#{'${rabbitmq.childupp.new-pwa.queue}'}")
    public void receiveMessage(final NewPWAEvent pwa, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {

        logger.info("***********************************************");
        logger.info("Received message PWA New: " + pwa.toString());
        logger.info(" => Pseudo AUTOACK for PWA New Queue");
        logger.info("***********************************************");

        channel.basicAck(tag, false);

        ChildUpp childUpp = childUppRepository.findOne(pwa.getChildUppId());
        int childupp = childUpp.getId();

        for (Icon iconSize : icons) {
            try {
                byte[] bytes = IOUtils.toByteArray(pwa.getUrl());
                Path path = Files.createTempFile("file_from_pwa", iconSize.getName() + ".png");
                Files.write(path, bytes);

                imageMagickTransformer.transform(path, path, iconSize.getX(), iconSize.getY(), 120);
                awsS3Service.uploadPWA(path, childupp, iconSize.getName() + ".png");
            }
            catch (IOException e) {
                throw new IllegalStateException("Error trying to upload the url: " + pwa.getUrl() + " for childUppId: " + pwa.getChildUppId(), e);
            }
        }

        // register default push if not exists...
        if (pushAPI.getApp(childupp).getCode() != 0) {
            logger.info("ChildUpp: " + childUpp + " not exists in Push Service, registering...");
            pushAPI.setApp(RequestApp.create(childupp));
        }
    }

    /**
     * Method to listen the Childupp Inactive Event and check if we must delete the IPA
     * and/or APK and related services
     *
     * @param childUppInactiveEvent ChildUppInactiveEvent the childupp inactive identifier
     */
    @RabbitListener(queues = "#{'${rabbitmq.childupp.inactive.queue}'}")
    public void receiveMessage(final ChildUppInactiveEvent childUppInactiveEvent, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {

        logger.info("***********************************************");
        logger.info("Received message ChildUpp Inactive event : " + childUppInactiveEvent.toString());
        logger.info(" => Pseudo AUTOACK for ChildUpp Inactive Queue");
        logger.info("***********************************************");

        channel.basicAck(tag, false);

        int childUppId = childUppInactiveEvent.getChildUppId();

        // remove push because we have some limits
        logger.info("Checking if we must delete the PUSH Service for childUppId: " + childUppId);
        if (pushAPI.getApp(childUppId).getCode() == 0) {
            logger.info("ChildUppId: " + childUppId + " exists in PUSH Service");
            logger.info("We must DELETE the Push Service for childUppId:" + childUppId);
            ResponseApp result = pushAPI.app(childUppId).delete();
            logger.info("Result deleting app in PUSH server: " + result);
        }
        else {
            logger.info("ChildUppId: " + childUppId + " doesnt exists in PUSH Service");
        }

        logger.info("ChildUppId: " + childUppId + " processed correctly from the DELETE event");
    }

    /**
     * Method to listen the Childupp Restore Event and check if we must delete the IPA
     * and/or APK and related services
     *
     * @param childUppRestoreEvent ChildUppRestoreEvent the restore childupp identifier
     */
    @RabbitListener(queues = "#{'${rabbitmq.childupp.restore.queue}'}")
    public void receiveMessage(final ChildUppRestoreEvent childUppRestoreEvent, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {

        logger.info("***********************************************");
        logger.info("Received message ChildUpp Restore event : " + childUppRestoreEvent.toString());
        logger.info(" => Pseudo AUTOACK for ChildUpp Restore Queue");
        logger.info("***********************************************");

        channel.basicAck(tag, false);

        int childUppId = childUppRestoreEvent.getChildUppId();

        ChildUppOS android = Optional
                .ofNullable(childUppOsRepository.findOne(filterByChildUppIdAndOs(childUppId, OperatingSystemType.ANDROID.getValue())))
                .filter((e) -> e.getStatus().equals(StatusRequest.SOLVED))
                .orElse(null);

        ChildUppOS ios = Optional
                .ofNullable(childUppOsRepository.findOne(filterByChildUppIdAndOs(childUppId, OperatingSystemType.IOS.getValue())))
                .filter((e) -> e.getStatus().equals(StatusRequest.SOLVED))
                .orElse(null);

        // restore push
        logger.info("Checking if we must restore the Push Service for ChildUppId: " + childUppId);
        if (android != null || ios != null) {
            logger.info("ChildUppId: " + childUppId + " have Android or iOS Apps created");
            String bundleId = Optional.ofNullable(ios).map(ChildUppOS::getBundleId).orElse(null);
            String packageName = Optional.ofNullable(android).map(ChildUppOS::getBundleId).orElse(null);

            if (pushAPI.getApp(childUppId).getCode() != 0) {
                logger.info("ChildUppId: " + childUppId + " are removed from Push Service");
                logger.info("We must RESTORE the Push Service for ChildUppId:" + childUppId);
                ResponseApp result = pushAPI.setApp(RequestApp.create(childUppId).withBundleId(bundleId).withPackageName(packageName));
                logger.info("Result creating app in Push Service: " + result);
            }
            else {
                logger.info("ChildUppId: " + childUppId + " have the PUSH service already enabled, skiping...");
            }
        }
        else {
            logger.info("ChildUppId: " + childUppId + " dont have APK or IPA we dont need to restore anything");
        }

        logger.info("ChildUppId: " + childUppId + " processed correctly from the RESTORE event");
    }

    public static class Icon {

        private int x;
        private int y;

        public Icon(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public String getName() {
            return x + "x" + y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }
}
