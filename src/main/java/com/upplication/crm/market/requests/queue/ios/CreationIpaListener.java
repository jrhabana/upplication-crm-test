package com.upplication.crm.market.requests.queue.ios;

import com.rabbitmq.client.Channel;
import com.upplication.crm.market.requests.RequestsManager;
import com.upplication.crm.market.requests.domain.RequestIpaMessage;
import com.upplication.crm.market.requests.util.CreationStatusUtil;
import es.upplication.entities.type.StatusPublishRequest;
import es.upplication.entities.type.StatusRequest;
import es.upplication.type.OperatingSystemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;


@Component
public class CreationIpaListener {

    Logger logger = LoggerFactory.getLogger(CreationIpaListener.class);

    private final CreationStatusUtil creationStatusUtil;
    private final RequestsManager requestsManager;

    public CreationIpaListener(CreationStatusUtil creationStatusUtil, RequestsManager requestsManager) {
        this.creationStatusUtil = creationStatusUtil;
        this.requestsManager = requestsManager;
    }

    /**
     * Method to listen the Creation IPA Status Queue and set the specific status at the DB
     *
     * @param request PublishRequestResponse response all the information with the worker result
     */
    @RabbitListener(queues = "#{'${rabbitmq.ipa.creationStatus.queue}'}")
    public void receiveCreationIpaStatusMessage(final RequestIpaMessage request, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {
        logger.info("Received message Creation IPA Status for ChildUpp "+request.getChildUppId()+", version "+request.getVersion()+" and Bundle "+request.getBundleId());

        int childUppId = request.getChildUppId();

        if (request.isSuccess()) {
            // update status
            creationStatusUtil.updateOSAppInDB(childUppId, OperatingSystemType.IOS, request.getIpaUrl(), request.getBundleId(), request.getVersion(), request.getIcon(), StatusRequest.SOLVED);
            // send new message to Itunes Publish Exchange
            creationStatusUtil.sendItunesConnectPublication(request.getRequestId(), request.getBundleId(), request.getIpaUrl());
            // remove error
            requestsManager.setIpaErrorMessage( request.getRequestId(), null);
        }
        else {
            int requestId = request.getRequestId();
            requestsManager.changeIosStatus(requestId, StatusPublishRequest.INCORRECT);
            String completeErrorMsg = "Fallo en el paso al intentar crear el IPA\nERROR MSG: " + request.getErrorMsg();
            logger.warn("Error process at the Ipa Worker building Request ID: " + requestId, completeErrorMsg);
            requestsManager.setIpaErrorMessage(requestId, completeErrorMsg);
            // save ipa as error
            creationStatusUtil.updateChildUppStatusRequest(childUppId, OperatingSystemType.IOS, StatusRequest.ERROR);
        }

        logger.info("Sending ACK to Creation IPA Status Queue");
        channel.basicAck(tag, false);
    }

}
