package com.upplication.crm.market.requests.domain;

import es.upplication.entities.PublishRequest;
import es.upplication.entities.Screenshot;

import java.util.Date;
import java.util.List;

public class Request {

    private int id;
    private int childUppId;
    private Date date;
    private String childUppName;
    private String organizationName;
    private String apkUrl;
    private int androidVersion;
    private String ipaUrl;
    private int iosVersion;
    private PublishRequest info;
    private boolean iosPublished;
    private boolean androidPublished;
    private String iosMarketUrl;
    private String androidMarketUrl;
    private String androidPublicationDate;
    private String iosPublicationDate;
    private String iosBundleId;
    private String androidBundleId;
    private List<Screenshot> mobileScreenshots;
    private List<Screenshot> tabletScreenshots;
    private boolean ownIosSign;
    private boolean ownAndroidSign;
    private PublisherIos publisherIos;
    private PublisherAndroid publisherAndroid;


    public void setChildUppId(int childUppId) {
        this.childUppId = childUppId;
    }

    public int getChildUppId() {
        return childUppId;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setChildUppName(String childUppName) {
        this.childUppName = childUppName;
    }

    public String getChildUppName() {
        return childUppName;
    }

    public void setApkUrl(String apkUrl) {
        this.apkUrl = apkUrl;
    }

    public String getApkUrl() {
        return apkUrl;
    }

    public void setAndroidVersion(int androidVersion) {
        this.androidVersion = androidVersion;
    }

    public int getAndroidVersion() {
        return androidVersion;
    }

    public void setIpaUrl(String ipaUrl) {
        this.ipaUrl = ipaUrl;
    }

    public String getIpaUrl() {
        return ipaUrl;
    }

    public void setIosVersion(int iosVersion) {
        this.iosVersion = iosVersion;
    }

    public int getIosVersion() {
        return iosVersion;
    }

    public void setInfo(PublishRequest info) {
        this.info = info;
    }

    public PublishRequest getInfo() {
        return info;
    }

    public boolean isIosPublished() {
        return iosPublished;
    }

    public void setIosPublished(boolean iosPublished) {
        this.iosPublished = iosPublished;
    }

    public boolean isAndroidPublished() {
        return androidPublished;
    }

    public void setAndroidPublished(boolean androidPublished) {
        this.androidPublished = androidPublished;
    }

    public String getIosMarketUrl() {
        return iosMarketUrl;
    }

    public void setIosMarketUrl(String iosMarketUrl) {
        this.iosMarketUrl = iosMarketUrl;
    }

    public String getAndroidMarketUrl() {
        return androidMarketUrl;
    }

    public void setAndroidMarketUrl(String androidMarketUrl) {
        this.androidMarketUrl = androidMarketUrl;
    }

    public String getAndroidPublicationDate() {
        return androidPublicationDate;
    }

    public void setAndroidPublicationDate(String androidPublicationDate) {
        this.androidPublicationDate = androidPublicationDate;
    }

    public String getIosPublicationDate() {
        return iosPublicationDate;
    }

    public void setIosPublicationDate(String iosPublicationDate) {
        this.iosPublicationDate = iosPublicationDate;
    }

    @Override
    public boolean equals(Object object){
        if (object instanceof Request){
            Request called = (Request)object;
            if (called.getChildUppId() == this.childUppId){
                return true;
            }
        }
        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIosBundleId() {
        return iosBundleId;
    }

    public void setIosBundleId(String iosBundleId) {
        this.iosBundleId = iosBundleId;
    }

    public String getAndroidBundleId() {
        return androidBundleId;
    }

    public void setAndroidBundleId(String androidBundleId) {
        this.androidBundleId = androidBundleId;
    }

    public List<Screenshot> getMobileScreenshots() {
        return mobileScreenshots;
    }

    public void setMobileScreenshots(List<Screenshot> mobileScreenshots) {
        this.mobileScreenshots = mobileScreenshots;
    }

    public List<Screenshot> getTabletScreenshots() {
        return tabletScreenshots;
    }

    public void setTabletScreenshots(List<Screenshot> tabletScreenshots) {
        this.tabletScreenshots = tabletScreenshots;
    }

    public boolean isOwnIosSign() {
        return ownIosSign;
    }

    public void setOwnIosSign(boolean ownIosSign) {
        this.ownIosSign = ownIosSign;
    }

    public boolean isOwnAndroidSign() {
        return ownAndroidSign;
    }

    public void setOwnAndroidSign(boolean ownAndroidSign) {
        this.ownAndroidSign = ownAndroidSign;
    }

    public PublisherIos getPublisherIos() {
        return publisherIos;
    }

    public void setPublisherIos(PublisherIos publisherIos) {
        this.publisherIos = publisherIos;
    }

    public PublisherAndroid getPublisherAndroid() {
        return publisherAndroid;
    }

    public void setPublisherAndroid(PublisherAndroid publisherAndroid) {
        this.publisherAndroid = publisherAndroid;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }
}
