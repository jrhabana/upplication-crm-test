package com.upplication.crm.market.requests.domain;


public class PublisherAndroid {

    private String signatureKeyAlias;
    private String signaturePasswordAlias;
    private String signaturePasswordStore;
    private String signatureStoreUri;
    private String email;
    private String teamName;

    public String getSignatureKeyAlias() {
        return signatureKeyAlias;
    }

    public void setSignatureKeyAlias(String signatureKeyAlias) {
        this.signatureKeyAlias = signatureKeyAlias;
    }

    public String getSignaturePasswordAlias() {
        return signaturePasswordAlias;
    }

    public void setSignaturePasswordAlias(String signaturePasswordAlias) {
        this.signaturePasswordAlias = signaturePasswordAlias;
    }

    public String getSignaturePasswordStore() {
        return signaturePasswordStore;
    }

    public void setSignaturePasswordStore(String signaturePasswordStore) {
        this.signaturePasswordStore = signaturePasswordStore;
    }

    public String getSignatureStoreUri() {
        return signatureStoreUri;
    }

    public void setSignatureStoreUri(String signatureStoreUri) {
        this.signatureStoreUri = signatureStoreUri;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
