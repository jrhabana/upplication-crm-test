package com.upplication.crm.market.requests.util;

import es.upplication.entities.type.CustomerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.Normalizer;
import java.util.regex.Pattern;

@Component
public class FriendlyUrl {

    @Autowired
    private Environment env;

    /**
     * Get the friendly url for a webapp
     *
     * @param subdomain    Subdomain name
     * @param customerType Customer type
     * @return Full URL
     */
    public URL getURL(String subdomain, CustomerType customerType) {
        String domain = "." + env.getProperty("upplication.childupp.domain");

        if (customerType.equals(CustomerType.ONO)) {
            domain = ".vodafone" + domain;
        }

        try {
            return new URL("http://" + subdomain + domain);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Error trying to build the URL with subdomain name: " + subdomain, e);
        }
    }

    /**
     * Get the friendly url for a webApp
     *
     * @param name String mandatory
     * @return String subdomain
     */
    public String getSubdomainFromName(String name) {
        // Remove accents
        String nfdNormalizedString = Normalizer.normalize(name, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        name = pattern.matcher(nfdNormalizedString).replaceAll("");

        // Remove non-alphanumeric characters and transform the string to lowercase
        name = name.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
        // Remove whitespaces
        name = name.replace(" ", "");

        return name;
    }

}
