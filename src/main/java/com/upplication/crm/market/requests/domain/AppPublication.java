package com.upplication.crm.market.requests.domain;


public class AppPublication {

    private String userName;
    private String userMail;
    private String userLang;
    private String marketUrl;
    private String os;
    private String appName;
    private String organizationName;

    public AppPublication() {
    }

    public String getUserName() {
        return this.userName;
    }

    public AppPublication setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getUserMail() {
        return this.userMail;
    }

    public AppPublication setUserMail(String userMail) {
        this.userMail = userMail;
        return this;
    }

    public String getUserLang() {
        return this.userLang;
    }

    public AppPublication setUserLang(String userLang) {
        this.userLang = userLang;
        return this;
    }

    public String getMarketUrl() {
        return this.marketUrl;
    }

    public AppPublication setMarketUrl(String marketUrl) {
        this.marketUrl = marketUrl;
        return this;
    }

    public String getOs() {
        return this.os;
    }

    public AppPublication setOs(String os) {
        this.os = os;
        return this;
    }

    public String getAppName() {
        return this.appName;
    }

    public AppPublication setAppName(String appName) {
        this.appName = appName;
        return this;
    }

    public String getOrganizationName() {
        return this.organizationName;
    }

    public AppPublication setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
        return this;
    }

}
