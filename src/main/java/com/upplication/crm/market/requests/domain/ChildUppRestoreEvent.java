package com.upplication.crm.market.requests.domain;


import java.io.Serializable;
import java.net.URI;

public class ChildUppRestoreEvent implements Serializable {

    private int childUppId;

    public ChildUppRestoreEvent() { }

    public ChildUppRestoreEvent(int childUppId) {
        this.childUppId = childUppId;
    }

    public int getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(int childUppRequest) {
        this.childUppId = childUppRequest;
    }

    @Override
    public String toString() {
        return "ChildUppRestoreEvent{" +
                " childUppId=" + childUppId +
                '}';
    }
}
