package com.upplication.crm.support.web;


import org.springframework.http.MediaType;

import java.nio.charset.Charset;

public abstract class HttpUtil {

    /**
     * Media Type: application/json;charset=UTF-8
     */
    public static final MediaType MEDIA_TYPE_APPLICATION_JSON = new MediaType("application", "json", Charset.forName("UTF-8"));
}
