package com.upplication.crm;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@ServletComponentScan
public class FenixApp {
    public static void main(String[] args) throws Exception {
        new SpringApplicationBuilder()
                .sources(FenixApp.class)
                .run(args);
    }
}
