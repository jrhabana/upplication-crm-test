package com.upplication.crm.sales;

import com.upplication.crm.support.web.PageWrapper;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class SalesController {

    private static final String SALES_VIEW = "sales/index";

    @Autowired
    SalesManager salesManager;

    @RequestMapping(value = "sales")
    public String sales(Model model, @PageableDefault(sort = {"date"}, direction = Sort.Direction.DESC, value = 20) Pageable pageable,
                        @RequestParam(value = "start", required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                        @RequestParam(value = "end", required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                        @RequestParam(value = "childUppId", required = false) Integer childUppId) {

        PageWrapper<SaleLine> page =
                new PageWrapper<> (salesManager.getSales(pageable, start, end, childUppId), "sales");

        model.addAttribute("page", page);
        model.addAttribute("start", start);
        model.addAttribute("end", end);
        model.addAttribute("childUppId", childUppId);
        return SALES_VIEW;
    }

    //FIXME: use custom view resolver
    @RequestMapping(value = "sales/export.csv", method = RequestMethod.POST)
    @ResponseBody
    public String salesExportCSV(@Valid @ModelAttribute SalesAdminForm adminForm, HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=" + LocalDate.now().toString() + ".csv");
        StringBuilder stringBuilder = new StringBuilder();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyyy");
        stringBuilder.append("Start, End, Payment, Childupp Id, ChildUpp Name, UserName, Amount, Bill Number\n");
        List<SaleLine> results = salesManager.getSales(adminForm.getStart(), adminForm.getEnd(), adminForm.getChildUppId());

        for (SaleLine result : results) {
            stringBuilder.append(String.format("%s, %s, %s, %s, %s, %s, %s, %s\n",
                    df.format(result.getStart()), df.format(result.getEnd()), df.format(result.getDay()),
                    result.getChildUppId(), result.getChildUppName(), result.getUserName(),
                    result.getAmount(), result.getBillNumber()));
        }

        return stringBuilder.toString();
    }
}
