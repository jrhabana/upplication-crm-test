package com.upplication.crm.defaulters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultersController {

    private static final String DEFAULTERS_VIEW = "defaulters/index";

    @Autowired
    DefaultersManager manager;

    @RequestMapping(value = "defaulters")
    public String defaulters(Model model) {
        model.addAttribute("results", manager.getDefaulters());
        return DEFAULTERS_VIEW;
    }

}
