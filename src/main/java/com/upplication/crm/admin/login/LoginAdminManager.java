package com.upplication.crm.admin.login;

import com.upplication.crm.repository.ChildUppRepository;
import com.upplication.crm.repository.WebUserRepository;
import com.upplication.crm.repository.WebUserSpecification;
import com.upplication.crm.support.web.MessageHelper;
import es.upplication.entities.ChildUpp;
import es.upplication.entities.ChildUppOSId;
import es.upplication.entities.OS;
import es.upplication.entities.WebUser;
import es.upplication.entities.type.CustomerType;
import es.upplication.security.TokenAdminLogin;
import es.upplication.security.TokenLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.criteria.Predicate;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

import static com.upplication.crm.repository.ChildUppOsSpecification.filterByChildUppIdAndOs;
import static com.upplication.crm.repository.ChildUppSpecification.filterByName;
import static com.upplication.crm.repository.ChildUppSpecification.filterByNotDeleted;
import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class LoginAdminManager {

    @Autowired
    TokenAdminLogin tokenAdminLogin;
    @Autowired
    TokenLogin tokenLogin;

    @Autowired
    ChildUppRepository childUppRepository;
    @Autowired
    WebUserRepository webUserRepository;

    @Transactional
    public String getSigningUrl(LoginAdminForm adminForm) {

        String token = tokenAdminLogin.create(TokenAdminLogin.PASSPHRASE);

        String type = "1";
        String field = adminForm.getWebuser();

        if (adminForm.getChildupp() != null && !adminForm.getChildupp().isEmpty()){
            type ="2";
            field = adminForm.getChildupp();
        }

        WebUser webUser = null;
        if (type.equals("2")) {
            // childupp id or name
            try {
                int idChildUpp = Integer.valueOf(field);
                ChildUpp childUpp = childUppRepository.findOne(idChildUpp);
                if (childUpp != null) {
                    webUser = childUpp.getOwner();
                }
            } catch (NumberFormatException e) {
                String childUppName = field;
                ChildUpp childUpp = childUppRepository.findOne(where(filterByName(childUppName)).and(filterByNotDeleted()));
                if (childUpp != null) {
                    webUser = childUpp.getOwner();
                }
            }
        } else {
            try {
                int idWebUser = Integer.valueOf(field);
                webUser = webUserRepository.findOne(idWebUser);
            } catch (NumberFormatException e) {
                String emailToFind = field;
                webUser = webUserRepository.findOne(where(WebUserSpecification.matchesCriteria(emailToFind, null, null, null, false)));
            }
        }

        if (webUser == null) {
            return null;
        }

        // encrypt
        field = tokenAdminLogin.create(field);
        type = tokenAdminLogin.create(type);

        switch (webUser.getAccount().getOrganization().getName()) {
            case "upplication": {
                if (webUser.getCustomerType() == CustomerType.UPPLICATION) {
                    return "https://dashboard.upplication.io/v2/admin/login?token=" + token + "&field=" + field + "&type=" + type;
                } else {
                    // Aplicateca use the same domain and dashboard but the old version.
                    // if the domain will be different (like a another product like wingu) then the organization must NOT be upplication
                    return "https://dashboard.upplication.io/v2/aplicateca/login?token=" + tokenLogin.create(webUser.getCustomerId());
                }
            }
            case "wingu": {
                return "https://dashboard.winguapps.io/v2/admin/login?token=" + token + "&field=" + field + "&type=" + type;
            }
            default: {
                return "https://dashboard.upplication.io/web/admin/login?token=" + token + "&field=" + field + "&type=" + type;
            }
        }
    }
}
