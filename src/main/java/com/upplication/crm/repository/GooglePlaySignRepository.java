package com.upplication.crm.repository;

import es.upplication.entities.GooglePlaySign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface GooglePlaySignRepository extends JpaRepository<GooglePlaySign, Integer>,
        JpaSpecificationExecutor<GooglePlaySign> {
}
