package com.upplication.crm.repository;


import es.upplication.entities.OS;
import org.springframework.data.jpa.domain.Specification;

public class OSSpecification {

    public static Specification<OS> filterByName(final String name) {
        return (parameter, query, cb) -> cb.equal(parameter.<String>get("nameOS"), name);
    }

}
