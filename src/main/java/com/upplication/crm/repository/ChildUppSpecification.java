package com.upplication.crm.repository;

import es.upplication.entities.ChildUpp;
import es.upplication.entities.PaymentOrder;
import es.upplication.entities.WebUser;
import es.upplication.entities.type.CustomerType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Date;

public class ChildUppSpecification {

    public static Specification<ChildUpp> filterLikeName(final String name) {
        return (childUpp, query, cb) -> cb.like(childUpp.<String>get("name"), "%" + name + "%");
    }

    public static Specification<ChildUpp> filterByName(final String name) {
        return (childUpp, query, cb) -> cb.equal(childUpp.<String>get("name"), name);
    }

    public static Specification<ChildUpp> filterById(final int id) {
        return (childUpp, query, cb) -> cb.equal(childUpp.<Integer>get("id"), id);
    }

    public static Specification<ChildUpp> filterByCustomerType(final CustomerType customerType) {
        return (childUpp, query, cb) -> {
            Join<ChildUpp, WebUser> owner = childUpp.join("owner");
            return cb.equal(owner.<CustomerType>get("customerType"), customerType);
        };
    }

    public static Specification<ChildUpp> filterByNotDeleted() {
        return (childUpp, query, cb) -> cb.isNull(childUpp.<Date>get("deleteDate"));
    }

    public static Specification<ChildUpp> filterByCreationDates(final Date start, final Date end) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("creationDate"), start), criteriaBuilder.lessThanOrEqualTo(root.<Date>get("creationDate"), end));
    }

    public static Specification<ChildUpp> filterByFirstPaymentDate(final Date start, final Date end) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("creationDate"), start), criteriaBuilder.lessThanOrEqualTo(root.<Date>get("creationDate"), end));
    }

}
