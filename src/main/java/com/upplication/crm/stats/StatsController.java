package com.upplication.crm.stats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

@Controller
public class StatsController {

    private static final String APLICATECA_INDEX = "stats/index";

    @Autowired
    private StatsManager statsManager;

    @RequestMapping(value = "stats")
    public String sales(Model model,
                        @RequestParam(value = "start", required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") java.util.Date start,
                        @RequestParam(value = "end", required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") java.util.Date end,
                        @RequestParam(value = "filterType", required = false, defaultValue = "ALL")  FilterType filterType) {

        model.addAttribute("start", start);
        model.addAttribute("end", end);
        model.addAttribute("filterType", filterType);

        List<StatsSales> news = statsManager.getSubscriptions(start, end, filterType);

        List<StatsSales> downs = statsManager.getUnsubscriptions(start, end, filterType);

        model.addAttribute("news", news);
        model.addAttribute("downs", downs);

        return APLICATECA_INDEX;
    }
}
