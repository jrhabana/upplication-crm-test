package com.upplication.crm.stats;

import com.upplication.crm.repository.*;
import com.upplication.crm.repository.dto.Subscription;
import es.upplication.type.PricingPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class StatsManager {

    @Autowired
    private SubscriptionQueries queries;

    /**
     * Get all new subscriptions accumulated by month not in trial
     * @param start Date, filter by start, if null return the last 6 months for now
     * @param end Date, filter by end
     * @param filterType {@link FilterType} ALL, ORGANIC (upplication), APLICATECA (telefonica), VODAFONE (ono)
     * @return List
     */
    @Transactional
    public List<StatsSales> getSubscriptions(Date start, Date end, FilterType filterType) {
        start = start == null ? Date.from(java.time.LocalDate.now().minusMonths(6).withDayOfMonth(1).atStartOfDay().toInstant(ZoneOffset.UTC)) : start;
        end = end == null ? Date.from(java.time.LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC)) : end;
        final java.time.LocalDate localStart = start.toInstant()
                .atZone(ZoneOffset.systemDefault())
                .toLocalDate();
        final java.time.LocalDate localEnd = end.toInstant()
                .atZone(ZoneOffset.systemDefault())
                .toLocalDate();

        // childupps
        Map<String, List<Subscription>> query = queries.getSubscriptions(start, end).stream().map(removeOldPricingNames()).collect(Collectors.groupingBy(Subscription::getPricingName));
        // childupp_requests
        Map<String, List<Subscription>> query2 = queries.getSubscriptionsChildUppRequests(start, end).stream().map(removeOldPricingNames()).collect(Collectors.groupingBy(Subscription::getPricingName));
        // merge in query
        query2.forEach(merge(query));

        boolean organic = filterType == FilterType.ALL || filterType == FilterType.ORGANIC;
        boolean aplicateca = filterType == FilterType.ALL || filterType == FilterType.APLICATECA;
        boolean vodafone = filterType == FilterType.ALL || filterType == FilterType.VODAFONE;

        return populate(localStart, localEnd, organic, aplicateca, vodafone, query);
    }

    /**
     * Get all new unsubscriptions accumulated by month not in trial
     * @param start Date, filter by start, if null return the last 6 months for now
     * @param end Date, filter by end
     * @param filterType {@link FilterType} ALL, ORGANIC (upplication), APLICATECA (telefonica), VODAFONE (ono)
     * @return List
     */
    @Transactional
    public List<StatsSales> getUnsubscriptions(Date start, Date end, FilterType filterType) {

        start = start == null ? Date.from(java.time.LocalDate.now().minusMonths(6).atStartOfDay().toInstant(ZoneOffset.UTC)) : start;
        end = end == null ? Date.from(java.time.LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC)) : end;
        final java.time.LocalDate localStart = start.toInstant()
                .atZone(ZoneOffset.systemDefault())
                .toLocalDate();
        final java.time.LocalDate localEnd = end.toInstant()
                .atZone(ZoneOffset.systemDefault())
                .toLocalDate();

        // childupps
        Map<String, List<Subscription>> query = queries.getUnsubscriptionsByUpgrade(start, end).stream().map(removeOldPricingNames()).collect(Collectors.groupingBy(Subscription::getPricingName));
        Map<String, List<Subscription>> query3 = queries.getUnsubscriptionsByRemoved(start, end).stream().map(removeOldPricingNames()).collect(Collectors.groupingBy(Subscription::getPricingName));
        // childupps_requests
        Map<String, List<Subscription>> query2 = queries.getUnsubscriptionsChildUppRequests(start, end).stream().map(removeOldPricingNames()).collect(Collectors.groupingBy(Subscription::getPricingName));
        // merge in query
        query2.forEach(merge(query));
        query3.forEach(merge(query));

        boolean organic = filterType == FilterType.ALL || filterType == FilterType.ORGANIC;
        boolean aplicateca = filterType == FilterType.ALL || filterType == FilterType.APLICATECA;
        boolean vodafone = filterType == FilterType.ALL || filterType == FilterType.VODAFONE;

        return populate(localStart, localEnd, organic, aplicateca, vodafone, query);
    }

    private Function<SubscriptionQuery, Subscription> removeOldPricingNames() {
        return query -> {
            Subscription r = query.toSubscription();
            r.setPricingName(removeOldPricingNames(r.getPricingName()));
            return r;
        };
    }

    private List<StatsSales> populate(java.time.LocalDate localStart, java.time.LocalDate localEnd, boolean organic, boolean aplicateca, boolean vodafone, Map<String, List<Subscription>> query) {
        List<StatsSales> result = getPaidPlans(organic, aplicateca, vodafone).stream().map(pricingPlan ->{
            StatsSales statsSales = new StatsSales();
            statsSales.setPricingName(pricingPlan.getValue());

            for (java.time.LocalDate localdate = localStart.withDayOfMonth(1);
                 localdate.compareTo(localEnd.withDayOfMonth(1)) <= 0; localdate = localdate.plusMonths(1)) {

                final LocalDate finalLocalDate = localdate;


                statsSales.getValues().put(
                        localdate,
                        query.getOrDefault(pricingPlan.getValue(), new ArrayList<>())
                                .stream()
                                .filter(s ->  s.getDate().equals(finalLocalDate))
                                .mapToInt(Subscription::getCount)
                                .sum());
            }

            statsSales.setTotal(statsSales.getValues().entrySet().stream().mapToInt(Map.Entry::getValue).sum());

            return statsSales;
        }).collect(Collectors.toList());

        StatsSales totalByDate = new StatsSales();
        totalByDate.setPricingName("TOTAL");
        for (java.time.LocalDate localdate = localStart.withDayOfMonth(1);
             localdate.compareTo(localEnd.withDayOfMonth(1)) <= 0; localdate = localdate.plusMonths(1)) {
            int total = 0;
            for (StatsSales s : result) {
                total += s.getValues().getOrDefault(localdate, 0);
            }
            totalByDate.getValues().put(localdate, total);
        }
        totalByDate.setTotal(totalByDate.getValues().entrySet().stream().mapToInt(Map.Entry::getValue).sum());

        result.add(totalByDate);
        return result;
    }

    private BiConsumer<String, List<Subscription>> merge(Map<String, List<Subscription>> query) {
        return (newPricingNameKey, newPricingDates) -> {
            query.merge(newPricingNameKey,
                    newPricingDates,
                    // if already exits the newPricingKey in the map (toAppendPricingDates is equal to newPricingDates)
                    (List<Subscription> pricingDates, List<Subscription> toAppendPricingDates) -> {

                        toAppendPricingDates.forEach(p -> {
                            boolean found= false;
                            for (Subscription p2 : pricingDates) {
                                if (p2.getDate().equals(p.getDate())) {
                                    p2.setCount(p2.getCount() + p.getCount());
                                    found = true;
                                }
                            }
                            if (!found) {
                                pricingDates.add(p);
                            }
                        });
                        return pricingDates;
                    });
        };
    }

    private List<PricingPlan> getPaidPlans(boolean organic, boolean aplicateca, boolean vodafone) {

        List<PricingPlan> result = new ArrayList<>();

        if (organic) {
            PricingPlan[] organicPricingPlan = new PricingPlan[] {
                    PricingPlan.BASIC,
                    PricingPlan.PROFESSIONAL,
                    PricingPlan.PREMIUM,
                    PricingPlan.CREATEYOURAPP,
                    PricingPlan.UNLIMITED,
                    PricingPlan.OFERTA_LANZAMIENTO,
                    PricingPlan.BEGINNER,
            };
            result.addAll(Arrays.asList(organicPricingPlan));
        }

        if (aplicateca) {
            PricingPlan[] aplicatecaPricingPlan = new PricingPlan[] {
                    PricingPlan.APLICATECA_BASIC,
                    PricingPlan.APLICATECA_PROFESSIONAL,
                    PricingPlan.APLICATECA_PREMIUM,
                    PricingPlan.APLICATECA_CREATEYOURAPP,
            };
            result.addAll(Arrays.asList(aplicatecaPricingPlan));
        }

        if (vodafone) {

            PricingPlan[] vodafonePricingPlan = new PricingPlan[] {
                    PricingPlan.ONO_BASIC,
                    PricingPlan.ONO_PROFESSIONAL,
                    PricingPlan.ONO_PREMIUM
            };
            result.addAll(Arrays.asList(vodafonePricingPlan));
        }

        return result;
    }

    private String removeOldPricingNames(String pricingName) {
        if (pricingName.endsWith(".2013")) {
            return pricingName.replace(".2013", "");
        }

        return pricingName;
    }
}
