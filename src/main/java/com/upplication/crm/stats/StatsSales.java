package com.upplication.crm.stats;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;

public class StatsSales {

    private String pricingName;
    private Map<LocalDate, Integer> values = new LinkedHashMap<>();
    private int total;

    public String getPricingName() {
        return pricingName;
    }

    public void setPricingName(String pricingName) {
        this.pricingName = pricingName;
    }

    public Map<LocalDate, Integer> getValues() {
        return values;
    }

    public void setValues(Map<LocalDate, Integer> values) {
        this.values = values;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
