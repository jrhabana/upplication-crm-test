package com.upplication.crm.webusers;

import es.upplication.entities.type.CustomerType;

import java.util.Date;

public class WebUser {

    private int id;
    private Date registerDate;
    private Date deleteDate;
    private String name;
    private String email;
    private String phone;
    private String customerId;
    private String loginUrl;
    private CustomerType customerType;

    private WebUser() {}

    public static WebUser fromEntity(es.upplication.entities.WebUser webUser, String loginUrl) {
        return new WebUser()
                .setId(webUser.getId())
                .setName(webUser.getName() != null && webUser.getSurname() != null ? webUser.getName() + " " + webUser.getSurname() : webUser.getName())
                .setCustomerId(webUser.getCustomerId())
                .setPhone(webUser.getPhone())
                .setLoginUrl(loginUrl)
                .setRegisterDate(webUser.getRegistrationDate())
                .setEmail(webUser.getEmail())
                .setDeleteDate(webUser.getDeleteDate())
                .setCustomerType(webUser.getCustomerType());
    }

    public int getId() {
        return id;
    }

    public WebUser setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public WebUser setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public WebUser setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public WebUser setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getCustomerId() {
        return customerId;
    }

    public WebUser setCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public WebUser setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
        return this;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public WebUser setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
        return this;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public WebUser setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
        return this;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public WebUser setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
        return this;
    }
}
