package com.upplication.crm.admin.login;

import com.upplication.crm.config.WebSecurityConfigurationAware;
import es.upplication.security.TokenAdminLogin;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import java.util.Collections;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class LoginAdminControllerTest extends WebSecurityConfigurationAware {

    private MockHttpSession sessionAuthenticated;

    @Autowired
    LoginAdminController loginAdminController;

    @Before
    public void setUp(){
        Authentication principal = authenticate();

        this.sessionAuthenticated = new MockHttpSession();
        this.sessionAuthenticated.setAttribute(
                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                new MockSecurityContext(principal));
    }

    @Test
    public void display_break_the_door_form() throws Exception {

        mockMvc.perform(get("/admin/login").session(sessionAuthenticated))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/login/index"))
                .andExpect(content().string(
                        allOf(
                                containsString("<div class=\"container main-content-wrapper\">")
                        ))
                )
                .andExpect(xpath("//form//input[@name=\"webuser\" and @type=\"text\"]").exists())
                .andExpect(xpath("//form//input[@name=\"childupp\" and @type=\"text\"]").exists())
                .andExpect(xpath("//form//button[@type=\"submit\"]").exists())
                .andExpect(xpath("//form[@action=\"/admin/login\" and @method=\"post\"]").exists());
    }

    @Test
    public void not_webuser_or_childupp_show_error_message() throws Exception {

        mockMvc.perform(post("/admin/login").session(sessionAuthenticated))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/login/index"))
                .andExpect(xpath("//span[@class=\"help-block\"]").exists());
    }

    @Test
    public void send_web_user_then_redirect_with_token() throws Exception {

        String loginUrl = "https://dashboard.upplication.io/web/admin/login?token=mock&field=mock&type=mock";
        loginAdminController.loginAdminManager = mock(LoginAdminManager.class);
        when(loginAdminController.loginAdminManager.getSigningUrl(any())).thenReturn(loginUrl);

        mockMvc.perform(post("/admin/login")
                .param("webuser", "guapo@guapo.soy")
                .session(sessionAuthenticated))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(loginUrl));
    }

    @Test
    public void send_childupp_then_redirect_with_token() throws Exception {

        String loginUrl = "https://dashboard.upplication.io/web/admin/login?token=mock&field=mock&type=mock";
        loginAdminController.loginAdminManager = mock(LoginAdminManager.class);
        when(loginAdminController.loginAdminManager.getSigningUrl(any())).thenReturn(loginUrl);

        mockMvc.perform(post("/admin/login")
                .param("childupp", "childupp name")
                .session(sessionAuthenticated))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(loginUrl));
    }

    // helpers

    private Authentication authenticate() {
        return new UsernamePasswordAuthenticationToken(createUser(), null, Collections.singleton(createAuthority()));
    }

    private User createUser() {
        return new User("mock", "mock", Collections.singleton(createAuthority()));
    }

    private GrantedAuthority createAuthority() {
        return new SimpleGrantedAuthority("mock");
    }

    public static class MockSecurityContext implements SecurityContext {

        private static final long serialVersionUID = -1386535243513362694L;

        private Authentication authentication;

        public MockSecurityContext(Authentication authentication) {
            this.authentication = authentication;
        }

        @Override
        public Authentication getAuthentication() {
            return this.authentication;
        }

        @Override
        public void setAuthentication(Authentication authentication) {
            this.authentication = authentication;
        }
    }
}
